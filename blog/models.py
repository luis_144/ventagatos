from django.db import models
from django.utils import timezone

class Producto(models.Model):

    idProducto =  models.AutoField( primary_key=True) 
    codigoBarra = models.IntegerField() 
    descripcion = models.CharField(max_length=50)
    marca = models.CharField(max_length=50)
    precioCosto = models.IntegerField()
    precioPublico = models.IntegerField()
    stock = models.IntegerField()
    activo = models.IntegerField()



	UNIQUE(descripcion)



    author = models.ForeignKey('auth.User', on_delete=models.CASCADE)

    title = models.CharField(max_length=200)

    text = models.TextField()

    created_date = models.DateTimeField(

    default=timezone.now)

    published_date = models.DateTimeField(

    blank=True, null=True)

    def publish(self):

        self.published_date = timezone.now()

        self.save()

    def __str__(self):

        return self.title

# Create your models here.
